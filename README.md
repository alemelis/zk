# zk

```console
$ zk -h
usage: zk.py [-h] [--add ADD] [--search] [--open] [--tag TAG]
             [--follow FOLLOW]

optional arguments:
  -h, --help            show this help message and exit
  --add ADD, -a ADD
  --search, -s
  --open, -o
  --tag TAG, -t TAG
  --follow FOLLOW, -f FOLLOW
```
