import os
import sys
import argparse
from datetime import datetime

EDITOR='vim'
ZKROOTDIR='$HOME/zk/brain/'

def gennewkey(date):
    return date.strftime('%y%m%d%H%M%S')
   
def makenewfolder(date):
    newfolder = f"{ZKROOTDIR}/{date.strftime('%y/%m/%d')}"
    if not os.path.isdir(newfolder): os.makedirs(newfolder)
    return newfolder

def writetitle(newzk, zkname):
    with open(newzk, 'w') as f:
        f.write(f'# {zkname}\n')
    
def addzk(zkname):
    date = datetime.now()
    newkey = gennewkey(date)
    newfolder = makenewfolder(date)
    newzk = os.path.join(newfolder, f'{newkey}{zkname}.zk')
    writetitle(newzk, zkname)
    os.system(f'{EDITOR} {newzk}')

def copyname(cmd):
    cmd += ['rev', "cut -d '/' -f1", 'rev',
            "cut -d '.' -f1", 'pbcopy']
    return cmd

def openfile(cmd):
    cmd = ' | '.join(cmd)
    os.system(f'{EDITOR} "$({cmd})"')

def findbytag(tag):
    return [f'rg "#"{tag} {ZKROOTDIR}', 'fzf',
            'cut -d ":" -f1']

def searchids():
    return [f'ls {ZKROOTDIR}/*/*/*/*.zk', 'fzf',
            'cut -d ":" -f1']

def followid(zkid):
    return [f'rg -F "[["{zkid}"]]" {ZKROOTDIR}', 'fzf',
            'cut -d ":" -f1']

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--add', '-a', type=str, default='')
    parser.add_argument('--search', '-s', action='store_true')
    parser.add_argument('--open', '-o', action='store_true')
    parser.add_argument('--tag', '-t', type=str, default='')
    parser.add_argument('--follow', '-f', type=str, default='')

    args = parser.parse_args()
        
    if args.add != '':
        addzk(args.add)

    cmd = ''

    if args.search:
        if args.tag == '':
            cmd = searchids()
        else:
            cmd = findbytag(args.tag)

    if args.follow != '':
        cmd = followid(args.follow)

    if args.open:
        openfile(cmd)
    else:
        cmd = copyname(cmd)
        os.system(' | '.join(cmd))
